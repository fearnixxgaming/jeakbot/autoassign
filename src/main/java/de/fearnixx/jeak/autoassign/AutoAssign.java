package de.fearnixx.jeak.autoassign;

import de.fearnixx.jeak.event.IQueryEvent;
import de.fearnixx.jeak.event.bot.IBotStateEvent;
import de.fearnixx.jeak.reflect.Config;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.JeakBotPlugin;
import de.fearnixx.jeak.reflect.Listener;
import de.fearnixx.jeak.teamspeak.QueryCommands;
import de.fearnixx.jeak.teamspeak.query.IQueryRequest;
import de.fearnixx.jeak.util.Configurable;
import de.mlessmann.confort.api.IConfig;
import de.mlessmann.confort.api.IConfigNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@JeakBotPlugin(id = "autoassign")
public class AutoAssign extends Configurable {

    private static final Logger logger = LoggerFactory.getLogger(AutoAssign.class);
    private final Map<Integer, Integer> groupsByChannel = new HashMap<>();

    @Inject
    @Config
    private IConfig configRef;

    public AutoAssign() {
        super(AutoAssign.class);
    }

    @Listener
    public void initialize(IBotStateEvent.IInitializeEvent initializeEvent) {
        if (!loadConfig()) {
            initializeEvent.cancel();
        } else {
            getConfig().getNode("assignments")
                    .optMap().orElseGet(Collections::emptyMap)
                    .forEach((cIDstr, sgNode) -> {
                        int cID = Integer.parseInt(cIDstr);
                        Integer sgID = sgNode.asInteger();

                        groupsByChannel.put(cID, sgID);
                    });
        }
    }

    @Listener
    public void clientMoved(IQueryEvent.INotification.IClientMoved event) {
        var sgID = groupsByChannel.get(event.getTargetChannelId());
        if (event.wasSelf() && groupsByChannel.containsKey(event.getTargetChannelId())) {
            if (!event.getTarget().getGroupIDs().contains(sgID)) {
                event.getConnection().sendRequest(
                        IQueryRequest.builder()
                                .command(QueryCommands.SERVER_GROUP.SERVERGROUP_ADD_CLIENT)
                                .addKey("sgid", sgID)
                                .addKey("cldbid", event.getTarget().getClientDBID())
                                .build()
                );
                logger.info("Server group {} assigned to client {}.", sgID, event.getTarget());
            } else {
                event.getConnection().sendRequest(
                        IQueryRequest.builder()
                                .command(QueryCommands.SERVER_GROUP.SERVERGROUP_DEL_CLIENT)
                                .addKey("sgid", sgID)
                                .addKey("cldbid", event.getTarget().getClientDBID())
                                .build()
                );
                logger.info("Server group {} removed from client {}.", sgID, event.getTarget());
            }


        }


    }

    @Override
    protected IConfig getConfigRef() {
        return configRef;
    }

    @Override
    protected String getDefaultResource() {
        return null;
    }

    @Override
    protected boolean populateDefaultConf(IConfigNode root) {
        root.getNode("assignments").setMap();
        return true;
    }

    @Override
    protected void onDefaultConfigLoaded() {
        saveConfig();
    }
}
